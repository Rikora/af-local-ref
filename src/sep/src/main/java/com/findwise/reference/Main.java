package com.findwise.reference;

import com.findwise.commons.jettywebapp.JettySettings;
import com.findwise.commons.jettywebapp.JettyWebServer;

/**
 * Bootstraps the SEP {@code capsule} application.
 *
 * <p>
 * The {@code main} method is invoked by {@code capsule} when running the
 * application standalone using {@code java -jar}
 * </p>
 *
 * @author johan.sjoberg
 */
public class Main {

    /**
     * The project {@code main} class starting up an embedded {@code jetty}
     * intended to run with {@code capsule} as a standalone runnable
     * war/jar-file.
     *
     * @param args command line arguments
     * @throws Exception if an error occurs starting the application
     */
    public static void main(String[] args) throws Exception {
        JettySettings settings = JettySettings
                .create("sep")
                .instanceName("sep")
                .extraClasspath("lib") // permit loading stages from ${fwhome}/sep/lib
                .http(8083)
                .withWebAppInitializerClassName("com.findwise.sep.application.config.WebAppInitializer") // required
                .enableGzipResponse()
                .enableAccessLog();
        JettyWebServer.builder(settings).build(args).run();
    }
}
