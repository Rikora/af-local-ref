package com.findwise.connect.api;

import com.findwise.connect.api.event.ContentEvent;
import com.findwise.connect.api.event.ContentQueue;
import com.findwise.connect.api.event.JobExecution;
import com.findwise.connect.core.state.ReadOnlyConnectorState;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TestJobExecution implements JobExecution {

    private List<ContentEvent> events;

    public TestJobExecution() {
        this.events = new ArrayList<>();
    }

    public List<ContentEvent> getEvents() {
        return events;
    }

    @Override
    public boolean hasChanged(String id, String signature) {
        return true;
    }

    @Override
    public boolean isStopping() {
        return false;
    }

    @Override
    public Date getStartTime() {
        return null;
    }

    @Override
    public Date getPreviousStartTime() {
        return null;
    }

    @Override
    public Date getPreviousEndTime() {
        return null;
    }

    @Override
    public Date getPreviousCompletedEndTime() {
        return null;
    }

    @Override
    public ContentQueue getContentQueue() {
        return null;
    }

    @Override
    public void publish(ContentEvent contentEvent) {
        this.events.add(contentEvent);

    }

    @Override
    public void publish(ContentEvent.ContentEventBuilder contentEventBuilder) {
        this.events.add(contentEventBuilder.build());
    }

    @Override
    public void purge() {

    }

    @Override
    public ReadOnlyConnectorState getStateRepository() {
        return null;
    }

    @Override
    public void reportError(String s, String s1, String s2) {

    }

    @Override
    public void reportError(Exception e, String s) {

    }

    @Override
    public <C> void storeConfig(C c) {

    }

    @Override
    public <C> Optional<C> retrieveConfig(Class<C> aClass) {
        return Optional.empty();
    }

    @Override
    public void deleteConfig() {

    }
}

