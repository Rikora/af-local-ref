package com.findwise.connect.http;

import com.findwise.connect.api.TestJobExecution;
import com.findwise.connect.api.event.ContentEvent;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class StatensccConnectorTest {

    private static final String ID = "Alingsås";
    private static final String URL = "https://www.statenssc.se/system/kontor/besokservicekontor/alingsasservicekontor.8974.html";
    private static final String BASE_URI = "https://www.statenssc.se/4.5075ee3c175652b6264168/12.5075ee3c175652b6264170.portlet";
    private TestJobExecution jobExecution;
    private StatensccConnector statensccConnector;
    private StatensccClient client;

    @Before
    public void setUp() throws IOException {
        this.statensccConnector = new StatensccConnector();
        this.client = new StatensccClient(this.getClass().getClassLoader().getResourceAsStream("ssc.html"), BASE_URI);
        this.jobExecution = new TestJobExecution();
    }

    @Test
    public void containsOfficeEvents() throws Exception {
        statensccConnector.internalRun(jobExecution, client);

        assertEquals(1, jobExecution.getEvents().size());
    }

    @Test
    public void extractOfficeEvent() throws Exception {
        statensccConnector.internalRun(jobExecution, client);

        ContentEvent result = jobExecution.getEvents().get(0);
        assertEquals(ID, result.getId());
        assertEquals(URL, result.getContent().getValue("url"));
    }
}
