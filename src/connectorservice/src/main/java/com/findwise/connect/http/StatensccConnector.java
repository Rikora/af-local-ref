package com.findwise.connect.http;

import com.findwise.connect.api.Signatures;
import com.findwise.connect.api.TraverseConnector;
import com.findwise.connect.api.annotation.Parameter;
import com.findwise.connect.api.event.ContentDocument;
import com.findwise.connect.api.event.ContentEvent;
import com.findwise.connect.api.event.JobExecution;
import com.google.common.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class StatensccConnector implements TraverseConnector {

    private static final Logger log = LoggerFactory.getLogger(StatensccConnector.class);

    @Parameter(name = "Url")
    private String url = "https://www.statenssc.se/4.5075ee3c175652b6264168/12.5075ee3c175652b6264170.portlet";

    @Parameter(name = "Css selector", description = "Css selector to retrieve json content")
    private String selector = "body div p";

    @Override
    public void run(JobExecution jobExecution) throws Exception {
        internalRun(jobExecution, new StatensccClient(this.url));
    }

    @VisibleForTesting
    void internalRun(JobExecution jobExecution, StatensccClient client) {
        StatensccParser parser = new StatensccParser(client.getDocument(), this.selector);

        Arrays.stream(parser.getOffices())
                .map(this::createContentEvent)
                .filter(contentEvent -> hasChanged(jobExecution, contentEvent))
                .forEach(jobExecution::publish);
    }

    private boolean hasChanged(JobExecution jobExecution, ContentEvent contentEvent) {
        return jobExecution.hasChanged(contentEvent.getId(), contentEvent.getSignature());
    }

    private ContentEvent createContentEvent(Office office) {
        String id = office.getName();
        String url = office.getUrl();
        ContentDocument.ContentDocumentBuilder documentBuilder = ContentDocument.create(id);
        Signatures.SignatureBuilder signatureBuilder = Signatures.builder();
        documentBuilder.addField("url", url);
        signatureBuilder.with(url);

        String signature = signatureBuilder.createHashed();
        log.trace("{} got signature {}", url, signature);
        return ContentEvent.newAddEvent(documentBuilder)
                .setSignature(signature)
                .build();
    }

    @Override
    public ConnectorMode getConnectorMode() {
        return ConnectorMode.STATELESS;
    }
}
