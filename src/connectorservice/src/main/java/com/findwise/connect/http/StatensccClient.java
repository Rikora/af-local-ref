package com.findwise.connect.http;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class StatensccClient {

    private Document document;

    public StatensccClient(String url) throws IOException {
        this.document = Jsoup.connect(url).get();
    }

    public StatensccClient(InputStream in, String baseUri) throws IOException {
        this.document = Jsoup.parse(in, StandardCharsets.UTF_8.toString(), baseUri);
    }

    public Document getDocument() {
        return document;
    }
}
