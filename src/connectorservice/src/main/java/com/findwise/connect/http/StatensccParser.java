package com.findwise.connect.http;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.util.List;

public class StatensccParser {

    private Document document;
    private String selector;

    public StatensccParser(Document document, String selector) {
        this.document = document;
        this.selector = selector;
    }

    public Office[] getOffices() {
        Elements content = this.document.select(this.selector);
        List<Node> nodes = content.get(0).childNodes();
        Office[] offices = new Office[nodes.size()];

        // Parse content
        for (int i = 0; i < nodes.size(); i++) {
            String name = nodes.get(i).childNode(0).outerHtml();
            String url = nodes.get(i).attributes().get("href");
            offices[i] = new Office(name, url);
        }
        return offices;
    }

    public Document getDocument() {
        return document;
    }

    public String getSelector() {
        return selector;
    }
}
