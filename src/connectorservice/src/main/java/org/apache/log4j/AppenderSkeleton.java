package org.apache.log4j;

import org.apache.log4j.spi.ErrorHandler;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.OptionHandler;

/**
 * The Norconex web crawler uses log4j logging instead of SLF4J. It turns out
 * that the {@code log4j-over-slf4j} does not implement everything to re-direct
 * logging to SLF4J instead. This class overrides a {@code log4j-over-slf4j}
 * class and also implements a method required for the Norconex logging to SLF4J
 * to work.
 *
 * <p>
 * Copy of {@link org.apache.log4j.AppenderSkeleton} that also implements
 * {@link org.apache.log4j.Appender}, which the {@code log4j-over-slf4j}
 * implementation does not do.
 * </p>
 *
 * @author martin.johansson
 */
public class AppenderSkeleton implements Appender, OptionHandler {

    @Override
    public void addFilter(Filter newFilter) {
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    @Override
    public void clearFilters() {
    }

    @Override
    public void close() {
    }

    @Override
    public void doAppend(LoggingEvent event) {
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setErrorHandler(ErrorHandler errorHandler) {
    }

    @Override
    public ErrorHandler getErrorHandler() {
        return null;
    }

    @Override
    public void setLayout(Layout layout) {
    }

    @Override
    public Layout getLayout() {
        return null;
    }

    @Override
    public void setName(String name) {
    }

    @Override
    public boolean requiresLayout() {
        return false;
    }

    @Override
    public void activateOptions() {
    }
}
