#!/bin/bash

#
# Compares dependencies against manually collected license files
# to detect if any licenses are missing or redundant
#


#
# Collect all artifact names from all dependencies
#
#      -n                    # don't print by default 
#      -e 's/..........//'   # shave off the first 10 chars 
#      -e 's/:compile$//p'   # only print lines containing compile 
#      -e '/findwise/d'      # drop Findwise dependencies
#      -e 's/^[^:]*://g'     # remove package name 
#      -e 's/:.*//g'         # remove anyhing trailing the artifact name
#      sort, uniq            # only display unique items
readarray -t dependencies <<< $(mvn dependency:list -DexcludeArtifactIds=filebeat-windows-x86_64,filebeat-linux-x86_64 2>/dev/null | sed \
      -n \
      -e 's/..........//' \
      -e 's/:compile$//p' \
      -e 's/:compile //p' \
      -e 's/:runtime$//p' \
      -e 's/:provided$//p' \
      | sed \
      -e '/findwise/d' \
      -e 's/^[^:]*://g' \
      -e 's/:.*//g' \
      | sort \
      | uniq)


#
# Collect all artifact names from license files
#
#    -e 's/\.[^.]*\.license\.txt//'  # strip license part
#    -e 's/\.notice\.txt//'          # strip notice part
#    sort, uniq                      # only display unique items
#
readarray -t licenses <<< $(ls fwhome/thirdparty_licences/java | sed \
    -e 's/\.[^.]*\.license\..*//' \
    -e 's/\.notice\..*//' \
    | sort \
    | uniq)


#
# Compare the differences between collected dependencies and license files
#
#      -1                   # suppress column 1 (lines unique to FILE1)
#      -2                   # suppress column 2 (lines unique to FILE2) 
#      -3                   # suppress column 3 (lines that appear in both files)

echo
echo "Dependencies that are missing license"
echo "======================================"
comm -23 <(printf '%s\n' "${dependencies[@]}") <(printf '%s\n' "${licenses[@]}")
echo


echo
echo "Licenses that are no longer in use"
echo "======================================"
comm -13 <(printf '%s\n' "${dependencies[@]}") <(printf '%s\n' "${licenses[@]}")
echo



#
# Print an overview of the different license types. 
#
# This is useful to make sure that there are no copyleft licences or other 
# incompatible licences. 
#
# The compiler dependency is excluded because it follows a different naming
# pattern because there are two versions of it used in different places. 
#
#    -e '/notice/d'                 # remove notice files
#    -e '/compiler/d'               # remove the two compiler dependencies
#    -e '/CUSTOM/d'                 # remove file names with CUSTOM license
#    -e '/MULTI/d'                  # remove file names with MULTI license
#    | grep -o '[^.]*\.license.txt' # extract [license-name].license.txt
#    | sed -e 's/\.license\.txt//'  # remove .license.txt
#    sort, uniq                     # only display unique items
#
echo
echo "Overview of licence types"
echo "======================================"
ls fwhome/thirdparty_licences/java | sed \
    -e '/notice/d' \
    -e '/CUSTOM/d' \
    -e '/MULTI/d' \
    -e '/compiler/d' \
    | grep -o '[^.]*\.license.txt' \
    | sed -e 's/\.license\.txt//' \
    | sort \
    | uniq
echo


#
# Print dependencies with a CUSTOM license. 
#
# This is useful since these dependencies should be kept to a minimum. 
#
echo
echo "Dependencies with CUSTOM license"
echo "======================================"
ls fwhome/thirdparty_licences/java | grep 'CUSTOM' | sed -e 's/\.CUSTOM.*//'



