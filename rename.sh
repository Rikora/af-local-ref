#!/bin/bash

#
# Bash script to rename groupId and artifactId
#
# Also removes scm tags since they point to the
# upstream repository
#

echo 
echo "Select the new groupId to replace 'reference' in 'com.findwise.reference' (tip: use project name)"
read -p "GroupID suffix: " groupIdSuffix 
echo
echo "Select the new artifactId to replace 'reference' in e.g. 'reference-indexsvc' (tip: use project name)"
read -p "ArtifactID prefix: " artifactIdPrefix
echo

echo "Locating pom files and renaming matching group- and artifactId"
find . -maxdepth 3 -not -path '*/target/*' -name "pom.xml" | xargs sed -i.backuprename "s/<groupId>com.findwise.reference<\/groupId>/<groupId>com.findwise.${groupIdSuffix}<\/groupId>/g; s/<artifactId>reference-\(.*\)<\/artifactId>/<artifactId>${artifactIdPrefix}-\1<\/artifactId>/g" 2>/dev/null
find . -name "*.backuprename" -type f -delete
echo "Done"

echo
echo "Done! Don't forget to update the SCM tags in the parent pom-file"
