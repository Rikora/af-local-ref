#!/bin/bash


#
# Utility script manage a running elasticsearch instance
# and in particular install required templates & pipelines
# The architecture is like this:
#
#                                   +----------------------------+
#                                   |       Elasticsearch        |
#                                   +----------------------------+
# +---------+       +----------+    | +-----------+    +-------+ |
# | i3 logs |`.  -> | Filebeat | -> | | Pipelines | -> | Index | |
# +---------+ |     +----------+    | +-----------+    +-------+ |
#   `---------+                     |                            |
#                                   +----------------------------+
#
# 1. FileBeat pick up i3 logs
# 2. Logs are sent to elasticsearch pipelines for processing
# 3. Logs are indexed in elasticsearch using the templates mapping
#
#
# Commands:
#
#   putTemplates   - Put all index templates from ./templates/
#                    Supplied are templates for i3 logs and analytics
#
#   putPipelines   - Put all pipelines from ./pipelines/
#                    Supplied are pipelines for i3 logs and analytics
#


#
# Configurables
#
basePath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
address="localhost:9200"
jsonHeader=-H'Content-Type: application/json'
authUser="admin"
authPassword="superadmin"
templatesPath="templates"
filebeatPath="../filebeat"
eventsPath="events"
instancesPath="${basePath}/instances.yml"
certGenOutPath="${basePath}/certificates.zip"
certsPath="certificates/"
cert="${certsPath}/node-1/node-1.p12"
certType="P12"
certPass="elastic"

for d in ../../../src/elasticsearch/target/unpacked/elasticsearch-*
do
  elasticPath="${d}"
done

curl() {
  echo -e "curl -sS ${1} ${auth} ${@:2}"
  command curl -sS ${1} ${auth} "${@:2}"
}

curlWithJsonHeader() {
  echo -e "curl -sS \"${jsonHeader}\" ${1} ${auth} ${@:2}"
  command curl -sS "${jsonHeader}" ${1} ${auth} "${@:2}"
}

function createIndex {
  echo -e "Creating index ${1}\n"
  curl -XPUT "${url}/${1}?pretty"
}

function showIndex {
  echo -e "Displaying elastic indices\n"
  curl "${url}/_cat/indices?v"
}

function deleteIndex {
  echo -e "Deleting index $1\n"
  curl -XDELETE "${url}/${1}?pretty"
}

function loglevel {
  echo -e "Changing root loglevel to ${1^^}\n"
  curlWithJsonHeader -XPUT "${url}/_cluster/settings?pretty" -d"
  {
    \"transient\" : {
      \"logger._root\" : \"${1^^}\"
    }
 }"
}

function putTemplates {
  templates=$(ls ${templatesPath} | grep template)
  echo -e "Putting all templates from ${templatesPath}\n"
  for template in ${templates}; do
    echo curlWithJsonHeader -XPUT "${url}/_template/${template%-template.json}?pretty" -d @${templatesPath}/${template}
    curlWithJsonHeader -XPUT "${url}/_template/${template%-template.json}?pretty" -d @${templatesPath}/${template}
    echo
  done
}

function putPipelines {
  echo -e "Putting all elasticsearch pipelines from ${filebeatPath}/module\n"
  pipelines=$(find ${filebeatPath}/module -type f -name '*.json')
  for pipeline in ${pipelines}; do
    echo curlWithJsonHeader -XPUT "${url}/_ingest/pipeline/$(basename ${pipeline%.json})?pretty" -d @${pipeline}
    curlWithJsonHeader -XPUT "${url}/_ingest/pipeline/$(basename ${pipeline%.json})?pretty" -d @${pipeline}
    echo
  done
}

function slowlog {
  echo -e "Changing search slog low threshold ${1,,} to $2 (use -1 to disable)\n"
  curlWithJsonHeader -XPUT "${url}/_all/_settings?pretty" -d"
  {
      \"index.search.slowlog.threshold.query.${1,,}\": \"$2\"
  }"
}

function createUser {
  echo -e "Creating user ${1}\n"
  curlWithJsonHeader -XPOST "${url}/_security/user/${1}?pretty" -d"
  {
    \"password\" : \"${2}\",
	\"roles\" : [${3}]
  }"
}

function changeUsersPassword {
  echo -e "Changing users ${1} password\n"

  curlWithJsonHeader -XPOST "${url}/_security/user/${1}/_password?pretty" -d"
  {
    \"password\" : \"${2}\"
  }"
}

function deleteUser {
  echo -e "Deleting user ${1}\n"

  curl -XDELETE "${url}/_security/user/${1}?pretty"
}

function enableUser {
  echo -e "Enabling user ${1}\n"

  curl -XPUT "${url}/_security/user/${1}/_enable?pretty"
}

function disableUser {
  echo -e "Disabling user ${1}\n"

  curl -XPUT "${url}/_security/user/${1}/_disable?pretty"
}

function createRole {
  echo -r "Creating role ${1}\n"

  curlWithJsonHeader -XPOST "${url}/_security/role/${1}?pretty" -d"${2}"
}

function deleteRole {
  echo -e "Deleting role ${1}\n"

  curl -XDELETE "${url}/_security/role/${1}?pretty"
}

function createRoleMapping {
  echo -e "Creating role mapping ${1}\n"

  curlWithJsonHeader -XPOST "${url}/_security/role_mapping/${1}?pretty" -d"${2}"
}

function deleteRoleMapping {
  echo -e "Deleting role mapping ${1}\n"

  curl -XDELETE "${url}/_security/role_mapping/${1}?pretty"
}

function callElasticScript {
  if [[ ${isWindows} == 1 ]];
    then
	  ./${elasticPath}/bin/${1}.bat ${2}
	else
	  bash ${elasticPath}/bin/${1} ${2}
  fi
}

function generateCertificate {
  echo -e "elasticsearch-certutil" "cert --silent --in ${1} --out ${2} --pass ${4}"
  callElasticScript "elasticsearch-certutil" "cert --silent --in ${1} --out ${2} --pass ${4}"
  unzip -o -d ${3} ${2}
  rm ${2}
}

function generateKeystore {
  echo -e "creating new elasticsearch.keystore"
  echo "y" | callElasticScript "elasticsearch-keystore" "create"
  echo -e "Adding xpack.security.transport.ssl.keystore.secure_password to elasticsearch.keystore"
  echo "${certPass}" | callElasticScript "elasticsearch-keystore" "add --stdin xpack.security.transport.ssl.keystore.secure_password"
  echo -e "Adding xpack.security.transport.ssl.truststore.secure_password to elasticsearch.keystore"
  echo "${certPass}" | callElasticScript "elasticsearch-keystore" "add --stdin xpack.security.transport.ssl.truststore.secure_password"
  if [[ ${SSL} == 1 ]]; then
    echo "Adding xpack.security.http.ssl.keystore.secure_password to elasticsearch.keystore"
    echo "${certPass}" | callElasticScript "elasticsearch-keystore" "add --stdin xpack.security.http.ssl.keystore.secure_password"
    echo "Adding xpack.security.http.ssl.truststore.secure_password to elasticsearch.keystore"
    echo "${certPass}" | callElasticScript "elasticsearch-keystore" "add --stdin xpack.security.http.ssl.truststore.secure_password"
  fi
  echo -e "Moving elasticsearch.keystore"
  cp ${elasticPath}/config/elasticsearch.keystore elasticsearch.keystore
}

function setupPasswords {
  echo -e "Setting up passwords"
  passwords=$(yes | callElasticScript "elasticsearch-setup-passwords" "auto -u ${url}" >&1 | grep "PASSWORD .*")

  apmSystemPasswordLine=$(echo "$passwords" | grep "PASSWORD apm_system = .*")
  kibanaPasswordLine=$(echo "$passwords" | grep "PASSWORD kibana = .*")
  logstashSystemPasswordLine=$(echo "$passwords" | grep "PASSWORD logstash_system = .*")
  beatsSystemPasswordLine=$(echo "$passwords" | grep "PASSWORD beats_system = .*")
  remoteMonitoringUserPasswordLine=$(echo "$passwords" | grep "PASSWORD remote_monitoring_user = .*")
  elasticPasswordLine=$(echo "$passwords" | grep "PASSWORD elastic = .*")

  elasticPassword=${elasticPasswordLine:19}
  echo -e "Generated elastic user password: ${elasticPassword}"
  auth="-u elastic:${elasticPassword}"
}

function printSummary {
  echo -e "Summary"
  echo -e "Created certificate = ${cert}"
  echo -e "PASSWORD certificate = ${certPass}"
  echo -e "PASSWORD ${authUser} = ${authPassword}"
  echo -e "${elasticPasswordLine}"
  echo -e "${kibanaPasswordLine}"
  echo -e "${beatsSystemPasswordLine}"
  echo -e "${logstashSystemPasswordLine}"
  echo -e "${remoteMonitoringUserPasswordLine}"
  echo -e "${apmSystemPasswordLine}"
}

function configureSecurity {
  echo -e "${SSL} ${BASIC}"
  auth=""
  url="http://"${address}
  echo -e "Moving elastic configuration without SSL"
  if [[ ${BASIC} == 1 ]]; then
    cp configurations/basic_auth_no_tls.yml elasticsearch.yml
  else
    cp configurations/no_auth_no_tls.yml elasticsearch.yml
  fi
  echo -e "Generating certificates"
  generateCertificate ${instancesPath} ${certGenOutPath} ${certsPath} ${certPass}
  generateKeystore
  echo -e "Now run elasticsearch and press ENTER"
  read
  setupPasswords
  createUser ${authUser} ${authPassword} \"superuser\"
  putPipelines
  putTemplates
  if [[ ${SSL} == 1 ]] ; then
    echo -e "Moving elastic configuration with SSL"
    if [[ ${BASIC} == 1 ]] ; then
      cp configurations/basic_auth_with_tls.yml elasticsearch.yml
    else
      cp configurations/no_auth_with_tls.yml elasticsearch.yml
    fi
    echo -e "Now restart elasticsearch and press ENTER"
    read
  fi
  echo -e "Configuring security finished"
  printSummary
}

function usage {
  echo "usage: "
  echo "  $0 [--basic use_basic_auth] [--ssl use_ssl] [-u username] [-p password] [-c cert_path] [-cp cert_password] [--cert-type] <command> [args]"
  echo
  echo "Note that [--basic] and [--ssl] will use default user credentials and certificates autogenerated with configureSecurity"
  echo "example: "
  echo "  $0 --basic --ssl configureSecurity"
  echo "  $0 --basic --ssl createIndex testing"
  echo "  $0 --basic --ssl showIndex testing"
  echo "  $0 --basic --ssl deleteIndex testing"
  echo
  echo "Commands"
  echo "  showIndex                                                       - Displays indices"
  echo "  createIndex <index>                                             - E.g. createIndex foo"
  echo "  deleteIndex <index>                                             - E.g. deleteIndex _all"
  echo "  putTemplates                                                    - Put all templates in directory ${templatesPath}"
  echo "  putPipelines                                                    - Put all pipelines in directories ${filebeatPath}/module"
  echo "  loglevel <loglevel>                                             - Changes the loglevel to one of INFO|WARN|ERROR|DEBUG"
  echo "  slowlog <loglevel> <timeout><s|ms>                              - Sets threshold of the slow log, e.g. slowlog warn 200ms"
  echo "  createUser <name> <password> <roles>                            - Creates user of specified name, password and roles"
  echo "  changeUsersPassword <name> <password>                           - Changes users password"
  echo "  deleteUser <name>                                               - Deletes user"
  echo "  enableUser <name>                                               - Enables user"
  echo "  disableUser <name>                                              - Disables user"
  echo "  createRole <name> <roleJSON>                                    - Creates specified role"
  echo "  deleteRole <name>                                               - Deletes role"
  echo "  createRoleMapping <name> <mappingJSON>                          - Creates specified role mapping"
  echo "  deleteRoleMapping <name>                                        - Deletes role mapping"
  echo "  generateCertificate <instances> <outPath> <certPath> <certPass> - Generates certificate to encrypt communications in Elasticsearch"
  echo "  configureSecurity                                               - Configures Elasticsearch security. Should be preceded by [--ssl] and [--basic] options.(Must be run with clean elasticsearch instance. Remove data/elasticsearch directory if launched before.)"
}

#
# Setting whether its Windows OS
#
case $(uname -s) in
  CYGWIN*) isWindows=1;;
  MINGW*)  isWindows=1;;
  *)       isWindows=0
esac

#
# Flags
#
BASIC=0
SSL=0

#
# Handling options
#
while [[ "$1" == -* ]]; do
  case "$1" in
    --ssl)
      SSL=1
    ;;

    --basic)
      BASIC=1
    ;;

    -u|--user|--username)
      shift
      authUser="$1"
    ;;

    -p|--pass|--password)
      shift
      authPassword="$1"
    ;;

	-c|--cert|--certificate)
      shift
      cert="$1"
	;;

	-cp|--cert-pass|--certificate-password)
      shift
      certPass="$1"
	;;

	--cert-type|--certificate-type)
      shift
      certType="$1"
	;;
  esac
  shift
done

#
# Setting url protocol
#
if [[ ${SSL} == 1 ]]; then
  url="https://"${address}
else
  url="http://"${address}
fi

#
# Setting authentication parameters
#
function setAuth {
  auth=""
  if [[ ${BASIC} == 1 ]] ; then
    auth="-u $authUser:$authPassword"
  fi
  if [[ ${SSL} == 1 ]] ; then
    auth="${auth} -k --cert-type ${certType} --cert ${cert}:${certPass}"
  fi
  echo "$auth"
}
setAuth

#
# Command selection
#
case "$1" in
  createIndex)
    createIndex $2
  ;;

  removeIndex|deleteIndex)
    deleteIndex $2
  ;;

  showIndex|showIndexes|showIndices)
    showIndex
  ;;

  putTemplates)
    putTemplates
  ;;

  putPipelines)
    putPipelines
  ;;

  loglevel)
    loglevel $2
  ;;

  deleteIndex)
    deleteIndex $2
  ;;

  slowlog)
    slowlog $2 $3
  ;;

  createUser)
	createUser "$2" "$3" "$4"
  ;;

  changeUsersPassword)
    changeUsersPassword "$2" "$3"
  ;;

  deleteUser)
    deleteUser "$2"
  ;;

  enableUser)
    enableUser "$2"
  ;;

  disableUser)
    disableUser "$2"
  ;;

  createRole)
    createRole "$2" "$3"
  ;;

  deleteRole)
    deleteRole "$2"
  ;;

  createRoleMapping)
    createRoleMapping "$2" "$3"
  ;;

  deleteRoleMapping)
    deleteRoleMapping "$2"
  ;;

  generateCertificate)
    generateCertificate "$2" "$3" "$4"
  ;;

  configureSecurity)
	configureSecurity
  ;;

  help|-help|--help|*)
      usage
  ;;
esac
exit 0
