@echo on
@SET parent=%~dp0

@REM Please provide which modules should be launched. It possible to run multiple with comma delimiter and without space
@REM between each further. Available: signals_clicks,signals_purchases,signals_search_result_feedbacks,signals_visits,
@REM query_completion_logs,elasticsearch_node_logs,elasticsearch_slow_logs,kibana_logs,i3_services_logs,signals_search,throttling_logs
@set $MODULES=signals_clicks,signals_purchases,signals_search_result_feedbacks,signals_visits,query_completion_logs,elasticsearch_node_logs,^
elasticsearch_slow_logs,kibana_logs,i3_services_logs,signals_search,throttling_logs

@REM set path variables
@call SET FW_LOG_DIR=..\..\..\logs
@call SET FW_DATA_DIR=..\..\..\data
@REM set path variables needed in filesets configuration
@call SET ES_LOG_DIR=%parent%%FW_LOG_DIR%\elasticsearch
@call SET KIBANA_LOG_DIR=%parent%%FW_LOG_DIR%\kibana
@call SET SOLR_LOGS_DIR=%parent%%FW_LOG_DIR%\solr
@REM set path variables
@call SET FILEBEAT_LOG_DIR=%FW_LOG_DIR%\filebeat
@call SET FILEBEAT_BIN_DIR="..\..\..\..\src\filebeat\target\unpacked\filebeat-*"
@REM loop to resolve path with wildcard
@for /f %%a in (%FILEBEAT_BIN_DIR%) do @set "FILEBEAT_BIN_DIR=%%~fa"

@if not exist "%FILEBEAT_LOG_DIR%" @call mkdir "%FILEBEAT_LOG_DIR%"

@echo Starting filebeat modules: %$MODULES% with logs at %FILEBEAT_LOG_DIR%

call %FILEBEAT_BIN_DIR%\filebeat.exe --modules %$MODULES% ^
 -c %parent%..\..\filebeat\filebeat.yml ^
 --path.home %parent%..\..\filebeat ^
 --path.config %parent%..\..\filebeat ^
 --path.data %parent%%FW_DATA_DIR%\filebeat ^
 --path.logs %parent%%FILEBEAT_LOG_DIR% ^
 > %FILEBEAT_LOG_DIR%\filebeat-stdout.log ^
 2> %FILEBEAT_LOG_DIR%\filebeat-stderr.log
